﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TomaszWojciechowskiLab5.Models;

namespace TomaszWojciechowskiLab5.Controllers
{
    public class MovieController : Controller
    {
        //
        // GET: /Movie/

        public LibraryDbContext context = new LibraryDbContext();

        public ActionResult Index()
        {
            Movie movie = (from mov in context.Movies
                           select mov).FirstOrDefault();

            return View(new Movie() { ReleaseDate = DateTime.Now });
        }

        public ActionResult ShowAll()
        {


            var query = (from mov in context.Movies
                         select mov).ToList();
            return View(query);
        }

        public ActionResult AddNewMovie(Movie newMovie)
        {
            context.Movies.Add(newMovie);
            context.SaveChanges();

            return RedirectToAction("ShowAll");
        }

        [HttpPost]
        public ActionResult DeleteById(int movieId)
        {
            context.Movies.Remove(context.Movies.Find(movieId));
            context.SaveChanges();

            return RedirectToAction("ShowAll");
        }

        public ActionResult UpdateById(int id)
        {
            var query = (from mov in context.Movies
                         where mov.ID == id
                         select mov).FirstOrDefault();
            return View(query);
        }

        [HttpPost]
        public ActionResult EditMovie(Movie editedMovie)
        {
            var movie = (from mov in context.Movies
                         where mov.ID == editedMovie.ID
                         select mov).FirstOrDefault();
            movie.Name = editedMovie.Name;
            movie.ReleaseDate = editedMovie.ReleaseDate;
            context.SaveChanges();

            return RedirectToAction("ShowAll");
        }
    }
}
