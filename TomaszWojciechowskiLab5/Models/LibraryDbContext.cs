﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TomaszWojciechowskiLab5.Models
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<LibraryDbContext>(new LibraryDbInitializer());
        }

        public virtual DbSet<Movie> Movies { get; set; }


    }
}