﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace TomaszWojciechowskiLab5.Models
{
    class LibraryDbInitializer : DropCreateDatabaseAlways<LibraryDbContext>
    {
        private LibraryDbContext context;

        protected override void Seed(LibraryDbContext con)
        {
            context = con;
            context.Movies.Add(new Movie() { Name = "Matrix", ReleaseDate = DateTime.Now.AddYears(-11) });
            context.Movies.Add(new Movie() { Name = "Matrix 2", ReleaseDate = DateTime.Now.AddYears(-10) });
            context.Movies.Add(new Movie() { Name = "Matrix 3", ReleaseDate = DateTime.Now.AddYears(-9) });
            context.Movies.Add(new Movie() { Name = "Piła", ReleaseDate = DateTime.Now.AddYears(-5) });
            context.Movies.Add(new Movie() { Name = "Killer", ReleaseDate = DateTime.Now.AddYears(-20) });
            context.SaveChanges();
            
            
            base.Seed(context);
        }


    }
}
