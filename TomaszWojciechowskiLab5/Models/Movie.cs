﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TomaszWojciechowskiLab5.Models
{
    public class Movie
    {
        public int ID { get; set; }

        [Display(Name="Nazwa")]
        public string Name { get; set; }
        
        [Display(Name="Data")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }


    }
}